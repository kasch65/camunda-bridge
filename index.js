require('dotenv').config()

console.log('Starting server...')

const trace = process.env.DEBUG === 'true' || false
const couchUri = process.env.COUCH_URI || 'http://localhost:5984'

const formDataPouch = require('./formDataPouch')
const camundaProcessDefPouch = require('./camundaProcessDefPouch')
const camundaProcessXmlPouch = require('./camundaProcessXmlPouch')
const camundaTaskPouch = require('./camundaTaskPouch')
const camundaService = require('./camundaService')

let refreshInterval

const _schedulePollFromCamunda = () => {
	// Reschedule camunda polling
	if (refreshInterval) {
		clearInterval(refreshInterval)
	}
	setTimeout(() => {
		_getFromCamunda()
		refreshInterval = setInterval(() => _getFromCamunda(), 10000)
	}, 10)
}

/**
 * Guesses the type of form variables
 * @param {*} v 
 */
const _getType = v => {
	if (v === true) return 'Boolean'
	else if (v === false) return 'Boolean'
	else return 'String'
}

/**
 * New form data encountered in pouch
 * Triggers a new process instannce
 * @param {*} newFormData 
 */
const _onNewFormData = newFormData => {
	if (newFormData.processInstanceId) {
		trace && console.debug('Not processing form data originating from task again.')
		return
	}
	// Prepare process variables for new process
	const processVars = {
		variables: {}
	}
	Object.entries(newFormData.values).forEach(([name, value]) => {
		processVars.variables[name] = { value: value, type: _getType(value) }
	})
	camundaService.startProcess(newFormData.procDefId, processVars)
		.then(() => {
			trace && console.debug('Deleting initial process data... %s', JSON.stringify(newFormData))
			formDataPouch.deleteItem(newFormData.id)
				.catch(err => console.error(err))
			_schedulePollFromCamunda()
			// New poll scheduled for soon because follow up tasks can be activated
		})
}

/**
 * Updated form data encountered in pouch
 * @param {*} updatedFormData 
 */
const _onUpdateFormData = updatedFormData => {
	const processVars = {
		modifications: {},
		deletions: []
	}
	if (!updatedFormData.values) {
		// Could be the filter
		return
	}
	Object.entries(updatedFormData.values).forEach(([name, value]) => {
		processVars.modifications[name] = { value: value, type: _getType(value) }
	})
	camundaService.updateTaskVariables(updatedFormData.id, processVars)
}

/**
 * Updated task data encountered in pouch
 * @param {*} changedTask 
 */
const _onUpdateTask = changedTask => {
	if (changedTask.id.startsWith('_design')) {
		return
	}
	if (changedTask.deleted) {
		camundaService.deleteProcessInstance(changedTask.processInstanceId)
		// Camunda will delete the tasks and deletion will get synchronized on next poll
	}
	else if (changedTask.completed) {
		camundaService.completeTask(changedTask.id)
			.then(() => _schedulePollFromCamunda())
			.catch(err => {
				const taskPatch = {completed: false, error: err}
				camundaTaskPouch.patchItem(changedTask.id, taskPatch)
			})
		// New poll scheduled for soon because follow up tasks can be activated
	}
	else {
		if (changedTask.assignee) {
			camundaService.claimTask(changedTask.id, changedTask.assignee)
		}
		else {
			camundaService.unclaimTask(changedTask.id)
		}
	}
}

/**
 * Initialize pouch and register change handlers
 */
const _initPouch = () => {
	formDataPouch.setDbUri(couchUri)
	formDataPouch.onChange(null, _onNewFormData, _onUpdateFormData, null)
	formDataPouch.sync()

	camundaProcessDefPouch.setDbUri(couchUri)
	camundaProcessDefPouch.onChange()
	camundaProcessDefPouch.sync()

	camundaProcessXmlPouch.setDbUri(couchUri)
	camundaProcessXmlPouch.onChange()
	camundaProcessXmlPouch.sync()

	camundaTaskPouch.setDbUri(couchUri)
	camundaTaskPouch.onChange(null, null, _onUpdateTask, null)
	camundaTaskPouch.sync()
}

_initPouch()

/**
 * Generic getter for camunda
 * @param {*} path 
 * @param {*} pouch
 * @returns a promise that resolves with an array of arrays: inPouchMissings, inCamundaMissings, inCamundaChanged
 */
const _getItemsFromCamunda = (path, pouch) => {
	return new Promise((resolve) => {
		camundaService.getJsonFromCamunda(path)
			.then(camundaItems => {
				pouch.getItems()
					.then(pouchItems => {
						const inPouchMissings = camundaItems.filter(camunda => !pouchItems.find(pouch => pouch.id === camunda.id))
						const inCamundaMissings = pouchItems.filter(pouch => !camundaItems.find(camunda => camunda.id === pouch.id))
						const inCamundaChanged = camundaItems.filter(camunda => pouchItems.find(pouch => pouch.id === camunda.id && camunda.assignee !== pouch.assignee))
						resolve([inPouchMissings, inCamundaMissings, inCamundaChanged])
					})
					.catch(err => {
						console.error('Pouch items error: ', err, path)
					})
			})
	})
}

/**
 * Requests items (process definitions and tasks) from camunda and processes them
 */
const _getFromCamunda = () => {
	_getItemsFromCamunda('/process-definition', camundaProcessDefPouch)
		.then(([inPouchMissings, inCamundaMissings]) => {
			inCamundaMissings.forEach(inCamundaMissing => {
				camundaProcessDefPouch.deleteItem(inCamundaMissing.id)
					.catch(err => console.error(err))
				camundaProcessXmlPouch.deleteItem(inCamundaMissing.id)
					.catch(err => console.error(err))
			})
			inPouchMissings.forEach(inPouchMissing => {
				inPouchMissing._id = inPouchMissing.id
				// Enrich process def
				const promises = []
				promises.push(camundaService.getJsonFromCamunda(`/process-definition/${inPouchMissing.id}/xml`))
				promises.push(camundaService.getJsonFromCamunda(`/process-definition/${inPouchMissing.id}/startForm`))
				Promise.all(promises)
					.then(([xml, startForm]) => {
						xml._id = inPouchMissing.id
						xml.tenantId = inPouchMissing.tenantId
						camundaProcessXmlPouch.addItem(xml)
							.catch(err => console.error(err))

						inPouchMissing.startForm = startForm
						camundaProcessDefPouch.addItem(inPouchMissing)
							.catch(err => console.error(err))
					})
					.catch(err => console.error(err))
			})
		})
	_getItemsFromCamunda('/task', camundaTaskPouch)
		.then(([inPouchMissings, inCamundaMissings, inCamundaChanged]) => {
			inCamundaMissings.forEach(inCamundaMissing => {
				camundaTaskPouch.deleteItem(inCamundaMissing.id)
					.then(pouch => trace && console.debug('Task deleted from pouch: %s', JSON.stringify(pouch)))
					.catch(err => console.error(err))
				formDataPouch.deleteItem(inCamundaMissing.id)
					.then(pouch => trace && console.debug('Form data deleted from pouch: %s', JSON.stringify(pouch)))
					.catch(err => console.error(err))
			})
			inPouchMissings.forEach(inPouchMissing => {
				inPouchMissing._id = inPouchMissing.id
				// Enrich task
				const promises = []
				promises.push(camundaService.getJsonFromCamunda(`/task/${inPouchMissing.id}/variables`))
				promises.push(camundaService.getJsonFromCamunda(`/task/${inPouchMissing.id}/identity-links`))
				Promise.all(promises)
					.then(([taskVariables, identityLinks]) => {
						inPouchMissing.identityLinks = identityLinks
						const formData = {
							_id: inPouchMissing.id,
							processInstanceId: inPouchMissing.processInstanceId,
							values: {}
						}
						Object.entries(taskVariables).forEach(e => {
							formData.values[e[0]] = e[1].value
						})
						formData.tenantId = inPouchMissing.tenantId
						formData.identityLinks = inPouchMissing.identityLinks
						formDataPouch.addItem(formData)
							.then(pouch => trace && console.debug('Form data added to pouch: %s', JSON.stringify(pouch)))
							.catch(err => console.error(err))
						camundaTaskPouch.addItem(inPouchMissing)
							.then(pouch => trace && console.debug('Task added to pouch: %s', JSON.stringify(pouch)))
							.catch(err => console.error(err))
					})
			})
			inCamundaChanged.forEach(camunda => {
				camundaTaskPouch.getItem(camunda.id)
					.then(() => {
						//console.debug('Different: ', JSON.stringify(camunda), JSON.stringify(pouch))
						camundaTaskPouch.patchItem(camunda.id, camunda)
							.catch(err => console.error(err))
					})
					.catch(err => console.error(err))
			})
		})
		.catch(err => console.error(err))
}

_schedulePollFromCamunda()
