const PouchDB = require('pouchdb-node')
const pouchBasics = require('./pouchBasics')

const dbName = 'bpe-formdata'
let localDb = new PouchDB(dbName, { auto_compaction: true })

const sorters = [(r1, r2) => new Date(r1.edit[r1.edit.length - 1].date) - new Date(r2.edit[r2.edit.length - 1].date)]
let remoteDb

const setDbUri = remoteUri => remoteDb = new PouchDB(remoteUri + '/' + dbName)

const addItem = item => pouchBasics.addItem(localDb, item)

const getItems = (selector, filter) => pouchBasics.getItems(localDb, sorters, selector, filter)

const getItem = id => pouchBasics.getItem(localDb, id)

const patchItem = (id, patch) => pouchBasics.patchItemAwait(localDb, id, patch)

const deleteItem = id => pouchBasics.deleteItem(localDb, id)

const onChange = (setItems, onAddItem, onPatchItem, onDeleteItem) => pouchBasics.onChange(localDb, sorters, setItems, onAddItem, onPatchItem, onDeleteItem)

const sync = (filter, filterParams) => pouchBasics.sync(localDb, remoteDb, filter, filterParams)

const stop = () => pouchBasics.stop(localDb)

module.exports = { setDbUri, addItem, getItems, getItem, patchItem, deleteItem, onChange, sync, stop }