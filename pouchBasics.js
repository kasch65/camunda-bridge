require('dotenv').config()

const trace = process.env.DEBUG === 'true' || false

const sort = (sorters, array) => {
	let tmp = array
	sorters.forEach(sorter => tmp = tmp.sort(sorter))
	return tmp
}

const addItem = (db, item) => {
	const clone = { ...item }
	delete clone.id
	delete clone.rev
	return new Promise((resolve, reject) => {
		db.post(clone)
			.then(result => {
				clone.id = result.id
				clone.rev = result.rev
				resolve(clone)
			})
			.catch(err => reject(err))
	})
}

const getItems = (db, sorters, selector) => {
	if (selector) {
		return new Promise((resolve, reject) => {
			db.find({
				selector
			})
				.then(result => {
					console.debug(result)
					const objects = sort(sorters, result.docs.filter(i => (i.id && !i.id.startsWith('_design/')))
						.map(doc => {
							doc.id = doc._id
							delete doc._id
							doc.rev = doc._rev
							delete doc._rev
							return doc
						}))
					resolve(objects)
				})
				.catch(err => reject(err))
		})
	}
	else {
		return new Promise((resolve, reject) => {
			db.allDocs({ include_docs: true, descending: true })
				.then(result => {
					const objects = sort(sorters, result.rows.filter(i => (i.id && !i.id.startsWith('_design/')))
						.map(row => {
							const doc = row.doc
							doc.id = doc._id
							delete doc._id
							doc.rev = doc._rev
							delete doc._rev
							return doc
						}))
					resolve(objects)
				})
				.catch(err => reject(err))
		})
	}
}

const getItem = (db, id) => {
	return new Promise((resolve, reject) => {
		db.get(id)
			.then(result => {
				result.id = result._id
				delete result._id
				result.rev = result._rev
				delete result._rev
				return resolve(result)
			})
			.catch(err => reject(err))
	})
}

const patchItem = (db, id, patch) => {
	return new Promise((resolve, reject) => {
		db.get(id)
			.then(old => {
				const clone = { ...old, ...patch }
				delete clone.id
				db.put(clone)
					.then(result => {
						clone.id = result.id
						delete clone._id
						clone.rev = result.rev
						delete clone._rev
						return resolve(clone)
					})
					.catch(err => reject(err))
			})
			.catch(err => reject(err))
	})
}

const patchItemAwait = async (db, id, patch) => {
	const old = await db.get(id)
	const clone = { ...old, ...patch }
	delete clone.id
	const result = await db.put(clone)
	clone.id = result.id
	delete clone._id
	clone.rev = result.rev
	delete clone._rev
	return clone
}

const deleteItem = (db, id) => {
	const patch = {
		_deleted: true
	}
	// See https://pouchdb.com/2015/04/05/filtered-replication.html
	//console.debug('Deleting item: ', id, ' from ', db.name)
	return patchItem(db, id, patch)
}

const onChange = (db, sorters, setItems, onAddItem, onPatchItem, onDeleteItem) => {
	db.changes({
		since: 0,
		live: true,
		include_docs: true
	})
		.on('change', change => {
			//console.debug('change: ', change)
			if (!change.deleted) {
				const item = change.doc
				item.id = item._id
				delete item._id
				item.rev = item._rev
				delete item._rev
				const itemRevParts = item.rev.split('-')
				const itemRev = Number(itemRevParts[0])
				if (setItems) {
					setItems(oldItemArray => {
						if (oldItemArray) {
							// Remove old item if exists
							let clone
							const oldItem = oldItemArray.find(c => c.id === item.id)
							if (itemRev === 1) {
								// new item
								//console.log('onNew: ', item)
								if (oldItem) {
									console.warn('Unexpected old item: ', oldItem.id, oldItem.rev)
									clone = oldItemArray.filter(c => c.id !== item.id)
									// Add new or changed item
									clone.push(item)
								}
								else {
									clone = oldItemArray.concat(item)
								}
								// Exclude design doc from sort
								const designDocs = clone.filter(i => (i.id && i.id.startsWith('_design/')))
								clone = sort(sorters, clone.filter(i => (i.id && !i.id.startsWith('_design/'))))
								clone.concat(designDocs)
							}
							else if (itemRev > 1) {
								// Patch
								//console.debug('onPatch: ', item)
								if (!oldItem) {
									console.warn('Missing old item to update with. Adding instead: ', item.id, item.rev)
									// Add new item
									clone = oldItemArray.concat(item)
									// Exclude design doc from sort
									const designDocs = clone.filter(i => (i.id && i.id.startsWith('_design/')))
									clone = sort(sorters, clone.filter(i => (i.id && !i.id.startsWith('_design/'))))
									clone.concat(designDocs)
								}
								else {
									//console.debug('Old and new revision: ', oldItem.rev, item.rev)
									const oldRevParts = oldItem.rev.split('-')
									if (Number(itemRevParts[0]) <= Number(oldRevParts[0])) {
										console.warn('A conflict has happened! A remote client has changed the item in the meantime!', oldItem.rev, item.rev)
									}
									// Update instead of replace item, no new sort required
									Object.keys(oldItem).forEach(key => delete oldItem[key])
									Object.assign(oldItem, item)
									clone = [...oldItemArray]
								}
							}
							// Return sorted list
							return clone
						}
						else {
							// New array
							return [item]
						}
					})
				}
				if (onAddItem && itemRev === 1) {
					onAddItem(item)
				}
				if (onPatchItem && itemRev > 1) {
					onPatchItem(item)
				}
			}
			else if (change.deleted) {
				if (setItems) {
					//console.log('onDelete: ', change.id)
					setItems(oldItemArray => {
						if (oldItemArray) {
							// Remove old item if exists
							const oldItem = oldItemArray.find(c => c.id === change.id)
							if (oldItem) {
								//console.debug('Old and new revision after delete: ', oldItem.rev, change.doc._rev)
								const oldRevParts = oldItem.rev.split('-')
								const newRevParts = change.doc._rev.split('-')
								if (Number(newRevParts[0]) <= Number(oldRevParts[0])) {
									console.warn('A conflict has happened! A remote client has changed the item in the meantime!', oldItem.rev, change.doc._rev)
								}
								return oldItemArray.filter(c => c.id !== change.id)
							}
							else {
								console.warn('Missing old item to delete: ', change.id)
								return oldItemArray
							}
						}
						else {
							return oldItemArray
						}
					})
				}
				if (onDeleteItem) {
					onDeleteItem(change.id)
				}
			}
		})
}

const sync = (localDb, remoteDb, filter, filterParams) => {
	// console.log('replication with ', remoteCouch)
	var opts = {
		live: true,
		retry: true
	}
	if (filter) {
		opts.filter = filter
	}
	if (filterParams) {
		opts.query_params = filterParams
	}

	global['pouchSyncFromHandler_' + localDb.name] = localDb.replicate.from(remoteDb, opts)
	global['pouchSyncFromHandler_' + localDb.name]
		.on('change', change => trace && console.debug('Change from remote received.', localDb.name, change))
		.on('paused', () => trace && console.debug('Change from remote paused.', localDb.name))
		.on('active', info => trace && console.debug('Change from remote resumed.', localDb.name, info))
		.on('complete', info => trace && console.debug('Change from remote canceled.', localDb.name, info))
		.on('error', error => trace && console.debug('Change from remote error.', localDb.name, error))

	global['pouchSyncToHandler_' + localDb.name] = localDb.replicate.to(remoteDb, {
		live: true,
		retry: true
	})
	global['pouchSyncToHandler_' + localDb.name]
		.on('change', change => trace && console.debug('Change to remote sent.', localDb.name, change))
		.on('paused', () => trace && console.debug('Change to remote paused.', localDb.name))
		.on('active', () => trace && console.debug('Change to remote resumed.', localDb.name))
		.on('complete', info => trace && console.debug('Change to remote canceled.', localDb.name, info))
		.on('error', error => trace && console.debug('Change to remote error.', localDb.name, error))
}

const stop = localDb => {
	if (global['pouchSyncFromHandler_' + localDb.name]) {
		global['pouchSyncFromHandler_' + localDb.name].cancel()
		delete global['pouchSyncFromHandler_' + localDb.name]
	}
	if (global['pouchSyncToHandler_' + localDb.name]) {
		global['pouchSyncToHandler_' + localDb.name].cancel()
		delete global['pouchSyncToHandler_' + localDb.name]
	}
}

module.exports = { addItem, getItems, getItem, patchItem, patchItemAwait, deleteItem, onChange, sync, stop }