const fetch = require('node-fetch')
require('dotenv').config()

const trace = process.env.DEBUG === 'true' || false
const camundaBaseUrl = process.env.CAMUNDA_URI || 'http://localhost:8095/engine-rest'

const _handleErrorResponse = (err) => {
	return new Promise((resolve) => {
		const error = { status: err.status, url: err.url, statusText: err.statusText }
		err.text()
			.then(body => {
				error.body = body
				console.trace('Camunda rest error: ', error)
				return resolve(error)
			})
			.catch(() => {
				console.trace('Camunda rest error: ', error)
				return resolve(error)
			})
	})
}

/**
 * General fetch
 * @param {*} path_ 
 */
const getJsonFromCamunda = path_ => {
	return new Promise((resolve) => {
		fetch(`${camundaBaseUrl}${path_}`)
			.then(response => {
				if (!response.ok) {
					throw response
				}
				response.json()
					.then(camundaItems => resolve(camundaItems))
			})
			.catch(err => _handleErrorResponse(err))
	})
}

const startProcess = (procDefId_, processVars_) => {
	return new Promise((resolve) => {
		const url = `${camundaBaseUrl}/process-definition/${procDefId_}/start`
		const config = {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify(processVars_)
		}
		fetch(url, config)
			.then(response => {
				if (!response.ok) {
					throw response
				}
				response.json()
					.then(newProcessInstanceResponse => {
						trace && console.debug('Process instance started: %s', JSON.stringify(newProcessInstanceResponse))
						return resolve(newProcessInstanceResponse)
					})
			})
			.catch(err => _handleErrorResponse(err))
	})
}

const updateTaskVariables = (taskId_, variableUpdates_) => {
	return new Promise((resolve) => {
		const url = `${camundaBaseUrl}/task/${taskId_}/variables`
		const config = {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify(variableUpdates_)
		}
		fetch(url, config)
			.then(response => {
				if (!response.ok) {
					throw response
				}
				trace && console.debug('Task variables updated: %s', url)
				return resolve(response)
			})
			.catch(err => _handleErrorResponse(err))
	})
}

const deleteProcessInstance = processInstanceId_ => {
	return new Promise((resolve) => {
		const url = `${camundaBaseUrl}/process-instance/${processInstanceId_}`
		const config = {
			method: 'DELETE'
		}
		fetch(url, config)
			.then(response => {
				if (!response.ok) {
					throw response
				}
				trace && console.debug('Process instance deleted: %s', url)
				return resolve(response)
			})
			.catch(err => _handleErrorResponse(err))
	})
}

const completeTask = taskId_ => {
	return new Promise((resolve, reject) => {
		const processVars = {
			variables: {}
		}
		const url = `${camundaBaseUrl}/task/${taskId_}/complete`
		const config = {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify(processVars)
		}
		fetch(url, config)
			.then(response => {
				if (!response.ok) {
					throw response
				}
				trace && console.debug('Task cpmpleted: %s', url)
				return resolve(response)
			})
			.catch(err => {
				_handleErrorResponse(err)
					.then(error => reject(error))
			})
	})
}

const claimTask = (taskId_, assignee_) => {
	return new Promise((resolve) => {
		const url = `${camundaBaseUrl}/task/${taskId_}/claim`
		const config = {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({ userId: assignee_ })
		}
		fetch(url, config)
			.then(response => {
				if (!response.ok) {
					throw response
				}
				trace && console.debug('Task claimed: %s, %s', url, assignee_)
				return resolve(response)
			})
			.catch(err => _handleErrorResponse(err))
	})
}

const unclaimTask = (taskId_) => {
	return new Promise((resolve) => {
		const url = `${camundaBaseUrl}/task/${taskId_}/unclaim`
		const config = {
			method: 'POST'
		}
		fetch(url, config)
			.then(response => {
				if (!response.ok) {
					throw response
				}
				trace && console.debug('Task unclaimed: %s', url)
				return resolve(response)
			})
			.catch(err => _handleErrorResponse(err))
	})
}

module.exports = { getJsonFromCamunda, startProcess, updateTaskVariables, deleteProcessInstance, completeTask, claimTask, unclaimTask }